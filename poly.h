/*
 * Bruce Cain
 *
 * Polynomial header file with function prototypes.
 */

#ifndef POLY_H
#define POLY_H

#include <stdbool.h>

typedef struct term polynomial;
typedef struct term
{
    int             coeff;
    unsigned int    exp;
    struct term    *next;
} term;

/* Creates polynomial term, malloc'd */
struct term    *term_create(
    int coeff,
    unsigned int exp);

/* Free polynomial's memory */
void            poly_destroy(
    polynomial * eqn);

/* Prints a polynomial */
void            poly_print(
    const polynomial * eqn);

/* Returns string version of polynomial */
char           *poly_to_string(
    const polynomial * p);

/* Adds two polynomials */
polynomial     *poly_add(
    const polynomial * a,
    const polynomial * b);

/* Subtracts two polynomials */
polynomial     *poly_sub(
    const polynomial * a,
    const polynomial * b);

/* Checks if two polynomials are equal */
bool            poly_equal(
    const polynomial * a,
    const polynomial * b);

/* Evaluates a polynomial with a given x value */
double          poly_eval(
    const polynomial * p,
    double x);

/* Transform a polynomial with user defined function */
void            poly_iterate(
    polynomial * p,
    void            (*transform) (struct term *));

/* Multiplies two polynomials */
polynomial     *poly_multi(
    const polynomial * a,
    const polynomial * b);
#endif
