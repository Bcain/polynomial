/*
* TDQC5
* Bruce Cain
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "poly.h"

void triple(polynomial *p);

int main(void)
{
    polynomial *p1 = term_create(2, 1);
    polynomial *p2 = term_create(3, 0);
    polynomial *p4 = term_create(4, 1);
    polynomial *p7 = term_create(-5, 0);
    polynomial *p3 = poly_add(p1, p2);
    polynomial *p5 = poly_add(p7, p4);
    polynomial *p6 = poly_multi(p3, p5);
    
    poly_print(p3);
    printf("\n");
    poly_print(p5);
    printf("\n");
    poly_print(p6);
    printf("\n");

    char *poly_string = poly_to_string(p6);
    printf("string %s\n", poly_string);

    free(poly_string);
    poly_destroy(p3);
    poly_destroy(p1);
    poly_destroy(p2);
    poly_destroy(p4);
    poly_destroy(p5);
    poly_destroy(p6);

    return 0;
}

void triple(polynomial *p)
{
    p->coeff *= 3;
}
