/*
 * TDQC5
 * Bruce Cain
 *
 * Source code to polynomial API
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "poly.h"

/*
 * Function   : POW
 * Return     : double
 * Parameters :
 *       base - A type double that is the number to be multiplied to itself
 *               exp times.
 *       exp  - A type unsigned int that is the number of times base is to
 *               be multiplied.
 * Purpose    : Calculate base raised to exp. 
 */
static double
POW(
    double base,
    unsigned int exp)
{
    double          ans = 1;

    for (unsigned int i = 0; i < exp; i++)
    {
        ans *= base;
    }

    return ans;
}

/*
 * Function   : digit_cnt
 * Return     : static int
 * Parameters : 
 *        num - The number to count its total digits.
 *
 * Purpose    : Count the number of digits and number has.
 */
static int
digit_cnt(
    int num)
{
    int             size = 1;

    while (num > 1)
    {
        num /= 10;
        size++;
    }

    return size;
}

/*
 * Function   : term_create
 * Return     : term *
 * Parameters :
 *      coeff - Type int, the coefficient of the term.
 *        exp - Type unsigned int, the exponent of the term.
 *
 * Purpose    : Create a term of a desired coefficient and exponent.
 */
term           *
term_create(
    int coeff,
    unsigned int exp)
{
    term           *node = calloc(1, sizeof(term));

    if (node == NULL)
    {
        return NULL;
    }

    if (node)
    {
        node->coeff = coeff;
        node->exp = exp;
        node->next = NULL;
    }

    return node;
}

/*
 * Function   : poly_destroy
 * Return     : void
 * Parameters : 
 *        eqn - Type polynomial *, the polynomial to be free'd.
 *
 * Purpose    : Free allocated memory.
 */
void
poly_destroy(
    polynomial * eqn)
{
    while (eqn)
    {
        term           *temp = eqn->next;

        free(eqn);
        eqn = temp;
    }
}

/*
 * Function   : poly_print
 * Return     : void
 * Parameters : 
 *        eqn - Type const polynomial *, the polynomial to be printed.
 *
 * Purpose    : Prints a polynomial to stdout.
 */
void
poly_print(
    const polynomial * eqn)
{
    static int      first_term = 1;
    int             coeff = 0;
    char            sign;

    if (!eqn)
    {
        return;
    }

    if (eqn->coeff)
    {
        if (eqn->coeff > 0)
        {
            sign = '+';
            coeff = eqn->coeff;
        }
        else
        {
            sign = '-';
            coeff = -1 * eqn->coeff;
        }

        if (first_term && sign == '+')
        {
            first_term = 0;
        }
        else if (first_term)
        {
            printf("%c", sign);
            first_term = 0;
        }
        else
        {
            printf("%c ", sign);
        }

        if (coeff != 1 || eqn->exp == 0)
        {
            printf("%d", coeff);
        }

        if (eqn->exp > 1)
        {
            printf("x^%d ", eqn->exp);
        }
        else if (eqn->exp == 1)
        {
            printf("x ");
        }
    }

    poly_print(eqn->next);
    first_term = 1;
}

/*
 * Function   : poly_add
 * Return     : polynomial *
 * Parameters :
 *          a - Type const polynomial *, left polynomial to be added.
 *          b - Type const polynomial *, right polynomial to be added.
 *
 * Purpose    : To add two polynomials together.
 */
polynomial     *
poly_add(
    const polynomial * a,
    const polynomial * b)
{
    polynomial     *poly_return = term_create(0, 0);

    /* Check if a and/or b are NULL */
    if (a == NULL && b == NULL)
    {
        poly_destroy(poly_return);
        return NULL;
    }
    else if (a == NULL)
    {
        poly_return->exp = b->exp;
        poly_return->coeff = b->coeff;
        poly_return->next = b->next;
    }
    else if (b == NULL)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff;
        poly_return->next = a->next;
    }
    else if (a->exp == b->exp)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff + b->coeff;
        poly_return->next = poly_add(a->next, b->next);
    }
    else if (a->exp > b->exp)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff;
        poly_return->next = poly_add(a->next, b);
    }
    else
    {
        poly_return->exp = b->exp;
        poly_return->coeff = b->coeff;
        poly_return->next = poly_add(a, b->next);
    }

    return poly_return;
}

/*
 * Function   : poly_sub
 * Return     : polynomial *
 * Parameters :
 *          a - Type const polynomial *, the left polynomial being subtracted.
 *          b - Type const polynomial *, the right polynomial being subtracted from a.
 *
 * Purpose    : To subtract two polynomials.
 */
polynomial     *
poly_sub(
    const polynomial * a,
    const polynomial * b)
{
    polynomial     *poly_return = term_create(0, 0);

    /* Check if a and/or b are NULL */
    if (a == NULL && b == NULL)
    {
        poly_destroy(poly_return);
        return NULL;
    }
    else if (a == NULL)
    {
        poly_return->exp = b->exp;
        poly_return->coeff = -1 * b->coeff;
        poly_return->next = b->next;
    }
    else if (b == NULL)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff;
        poly_return->next = a->next;
    }
    else if (a->exp == b->exp)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff - b->coeff;
        poly_return->next = poly_sub(a->next, b->next);
    }
    else if (a->exp > b->exp)
    {
        poly_return->exp = a->exp;
        poly_return->coeff = a->coeff;
        poly_return->next = poly_sub(a->next, b);
    }
    else
    {
        poly_return->exp = b->exp;
        poly_return->coeff = -1 * b->coeff;
        poly_return->next = poly_sub(a, b->next);
    }

    return poly_return;
}

/*
 * Function   : poly_equal
 * Return     : bool
 * Parameters :
 *          a - Type const polynomial *, the left polynomial being compared.
 *          b - Type const polynomial *, the right polynomial being compared.
 *
 * Purpose    : To determine if two polynomials are equal.
 */
bool
poly_equal(
    const polynomial * a,
    const polynomial * b)
{
    /* Check if a and/or b are NULL */
    if (a == NULL && b == NULL)
    {
        return true;
    }
    else if (a == NULL)
    {
        return false;
    }
    else if (b == NULL)
    {
        return false;
    }

    if (!a->coeff)
    {
        return poly_equal(a->next, b);
    }
    else if (!b->coeff)
    {
        return poly_equal(a, b->next);
    }

    if (a->exp == b->exp)
    {
        if (a->coeff == b->coeff)
        {
            return poly_equal(a->next, b->next);
        }
    }

    return false;
}

/*
 * Function   : poly_eval
 * Return     : double
 * Parameters :
 *          p - Type const polynomial *, the polynomial to be evaluated.
 *          x - Type double, the value to be subsituted into the polynomial.
 *
 * Purpose    : Evalute a given polynomial with a given x value.
 */
double
poly_eval(
    const polynomial * p,
    double x)
{
    if (p == NULL)
    {
        return 0;
    }

    return p->coeff * POW(x, p->exp) + poly_eval(p->next, x);
}

/*
 * Function   : poly_iterate
 * Return     : void
 * Parameters : 
 *          p - Type polynomial *, the polynomial to be iterated over through the
 *              transform function.
 *  transform - Type void function *, takes a single struct term* to do some operation
 *              on.
 *
 * Purpose    : Take user created functions that modify a polynomial.
 */
void
poly_iterate(
    polynomial * p,
    void            (*transform) (struct term *))
{
    if (p == NULL)
    {
        return;
    }

    poly_iterate(p->next, transform);

    if (p->coeff)
    {
        transform(p);
    }
}

/*
 * Function   : poly_to_string
 * Return     : char *
 * Parameters :
 *          p - Type const polynomial *, the polynomial to be changed into
 *              a string.
 *
 * Purpose    : Change a polynomial to a string.
 */
char           *
poly_to_string(
    const polynomial * p)
{
    char            sign;
    int             coeff, first_term = 1;
    char           *string = NULL, *return_str = NULL;

    if (p != NULL)
    {
        return_str = calloc(1, 3 + digit_cnt(p->coeff) + digit_cnt(p->exp));
    }
    else
    {
        return NULL;
    }

    for (; p != NULL; p = p->next)
    {
        if (p->coeff == 0)
        {
            continue;
        }

        string = calloc(1, 5 + digit_cnt(p->coeff) + digit_cnt(p->exp));

        if (p->coeff > 0)
        {
            sign = '+';
            coeff = p->coeff;
        }
        else
        {
            sign = '-';
            coeff = -1 * p->coeff;
        }

        if (first_term && sign == '-')
        {
            sprintf(string, "%c", sign);
        }
        else if (!first_term)
        {
            sprintf(string, "%c ", sign);
        }

        first_term = 0;
        strcat(return_str, string);


        if (coeff != 1 || p->exp == 0)
        {
            sprintf(string, "%d", coeff);
            strcat(return_str, string);
        }


        if (p->exp == 1)
        {
            sprintf(string, "x ");
            strcat(return_str, string);
        }
        else if (p->exp != 0)
        {
            sprintf(string, "x^%u ", p->exp);
            strcat(return_str, string);
        }


        return_str =
            realloc(return_str,
                    5 + digit_cnt(p->coeff) + digit_cnt(p->exp) +
                    strlen(return_str));

        free(string);
    }

    return return_str;
}

/*
 * Function   : poly_multi
 * Return     : polynomial *
 * Parameters :
 *          a - Type const polynomial *, left polynomial to be multiplied to b
 *          b - Type const polynomial *, right polynomial to be multiplied to a
 *
 * Purpose    : Multiply two polynomials together.
 */
polynomial     *
poly_multi(
    const polynomial * a,
    const polynomial * b)
{
    polynomial     *poly_return = term_create(0, 0);
    polynomial     *poly_tmp = term_create(0, 0);
    polynomial     *bholder = term_create(0, 0);
    polynomial     *tmp = bholder;

    bholder->coeff = b->coeff;
    bholder->exp = b->exp;
    bholder->next = b->next;

    for (; a != NULL; a = a->next)
    {
        for (bholder = tmp; bholder != NULL; bholder = bholder->next)
        {
            poly_tmp->coeff = a->coeff * bholder->coeff;
            poly_tmp->exp = a->exp + bholder->exp;
            poly_tmp->next = NULL;
            poly_return = poly_add(poly_tmp, poly_return);
        }
    }

    free(poly_tmp);

    return poly_return;
}
