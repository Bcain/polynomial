CC = gcc
CFLAGS = $(CF) -Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=512 -Wfloat-equal -Waggregate-return -Winline
OUT = poly_main
DEPS = poly.h
SRC = poly_main.c
ARC = poly.a

all: $(DEPS) $(ARC)
	@$(CC) $(CFLAGS) -o $(OUT) $(SRC) $(ARC)

poly.a: poly
poly: $(DEPS)
	@$(CC) $(CFLAGS) -c -o poly.o poly.c
	-@ar -r poly.a poly.o

debug: $(DEPS) $(ARC)
	@$(CC) -g $(CFLAGS) -o $(OUT) $(SRC) $(ARC)

clean:
	-@rm -rf *.o *.a
	-@rm -rf $(OUT)

